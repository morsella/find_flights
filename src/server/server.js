import path from 'path';
import express from 'express';
import config from '../../webpack.dev.config.js';
import webpack from 'webpack';
// const webpack = require('webpack');
const compiler = webpack(config);
const app = express(),
            DIST_DIR = __dirname,
            HTML_FILE = path.join(DIST_DIR, 'index.html');
app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler, {
    log: console.log,
    path: '/__webpack_hmr',
    heartbeat: 10 * 1000
}));
app.get('*', (req, res, next) => {
    compiler.outputFileSystem.readFile(HTML_FILE, (err, result) => {
    if (err) {
      return next(err)
    }
    res.set('content-type', 'text/html')
    res.send(result)
    res.end()
    })
  });
  const PORT = process.env.PORT || 3000;
  app.listen(PORT, () => {
      console.log(`App listening to ${PORT}....`)
      console.log('Press Ctrl+C to quit.')
  });