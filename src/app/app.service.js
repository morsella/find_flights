/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable no-console */

import axios from 'axios';
import {flights} from './data/flights';
import {FindFlight} from './flights/flights.service';

export const AppService = (string, selection) => {
  // const url = "/data/flights";
  // const params = `string=${string}&selection=${selection}`;
    return new Promise((resolve, reject) => {
    axios.get('https://dog.ceo/api/breeds/image/random')
    .then(res => {
      const flightsObj = JSON.parse(flights);
      const findFlight = FindFlight(string, selection, flightsObj);
      resolve(findFlight);
    }).catch((err)=>{
      reject(err);
    });
  });
}