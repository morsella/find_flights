/* eslint-disable no-console */
/* eslint-disable no-undef */
import {
    AppComponent
} from './app.component';

export const App = {
    init() {
        AppComponent.init();
    }
};