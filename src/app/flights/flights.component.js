/* eslint-disable no-console */
/* eslint-disable no-undef */
import {FlightListComponent} from './flight-list/flight-list.component';
import {FlightsModel} from './flights.model';
export const FlightsComponent  = {
    init(flights){
        this.appElement = document.querySelector('#flights_list');
        this.render(flights);
    },
    render(flights){
        let flightComponentHTML = '<section class="flight rw-card rw-card--caret">';
        if(flights.flights.length <= 0 && flights.search.trim() !== ''){
            flightComponentHTML += '<h3 class="rw-card__header">No Flighs Found</h3>';
        }else if(flights.flights.length > 0 && flights.search.trim() !== ''){
            flightComponentHTML += `<h3 class="rw-card__header">Fligths Found ${flights.flights.length}: ${FlightsModel(flights.search)}</h3>`;
        }
        flightComponentHTML += `<div class="rw-card__body">${FlightListComponent.render(FlightsModel(flights))}</div>`;
        flightComponentHTML += '</section>';
        this.appElement.innerHTML = flightComponentHTML;
    }
}