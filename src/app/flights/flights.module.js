import { FlightsComponent } from './flights.component';

export const FlightsModule = {
    init(flights) {
        FlightsComponent.init(flights);
    }
};