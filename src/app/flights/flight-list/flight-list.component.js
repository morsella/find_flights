/* eslint-disable no-undef */
/* eslint-disable no-console */
import {flightListTemplate} from './flight-list.template';
export const FlightListComponent = {
    render(model){
        const flightsHTML = model.flights.map(
            (flight) => {
                return flightListTemplate(flight);
            }).join("");
        return flightsHTML;
    }
}
