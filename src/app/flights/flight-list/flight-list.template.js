/* eslint-disable no-console */
/* eslint-disable no-undef */
export const flightListTemplate = flight => 
`<div class="rw-card__content">
    <h4 class="rw-card__header">Flight Number: ${flight.flightNumber}</h4>
    <p> airport: ${flight.airport} </p>
    <p> expected time: ${flight.expectedTime} </p>
    <p> flight identifier: ${flight.flightIdentifier} </p>
    <p> original time: ${flight.originalTime}</p>
</div><hr>`;
