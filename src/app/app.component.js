/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
/* eslint-disable no-undef */
import { appTemplate } from './app.template';
import { AppModel } from './app.model';
import {FlightsModule} from './flights/flights.module';
import {AppService} from  './app.service';

export const AppComponent = {
    init() {
        this.appElement = document.querySelector('#app');
        this.render();
        this.initEvents();
        this.renderReset(false);
    },
    initEvents() {
        const submitInput  = document.querySelector('.submit_input');
        const searchInput  = document.querySelector('.search_input');
        const resetInput = document.querySelector('.conditional_reset');
        searchInput.addEventListener('keyup', event => {
            let value = event.target.value;
            value = value.split(' ').join('').toLowerCase();
            if(this.validateInput(value) && value.length === 3){
                this.searchQuery(value);
            }else{
             this.reserSearch();   
            }
        });
        submitInput.addEventListener('click', event => {
            let value = searchInput.value;
            value = value.split(' ').join('').toLowerCase();
            if(this.validateInput(value) && value.length !== 3){
                this.searchQuery(value);
            }

        });
        resetInput.addEventListener('click', event => {
            searchInput.value = '';
            this.reserSearch();
        }); 
    },
    reserSearch(){
        FlightsModule.init({flights: [], search: ''});
        this.renderReset(false);
    },
    searchQuery(value){
        AppService(value, 'flightNumber').then(res => {
            FlightsModule.init({flights: res, search: value});  
        });
        this.renderReset(true);
    },
    validateInput(value){
        if(value.trim() === '' || value === null ) return false;
        return true;
    },
    renderReset(reset){
        const resetElement = document.querySelector('.conditional_reset');
        let template = null;
            if (reset) {
                template = '<button class="reset_search rw-button rw-button--clear">Clear Search</button>';
            }
        resetElement.innerHTML = template;
    },
    render() {
        this.appElement.innerHTML = appTemplate(AppModel);
    }
};