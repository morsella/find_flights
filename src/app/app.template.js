export const appTemplate = model =>`
    <h1 class="rw-schiphol-logo rw-schiphol-logo--wide rw-header__item">${model.title}</h1>
    <div>
        <form id="flight_search">
            <lable class="rw-input-label">Flight number:</lable>
            <input type="text" class="search_input rw-input-text" placeholder="KLM" />
        </form>
        <button class="submit_input rw-button">Search</button>
        <span class="conditional_reset"></span>
        <div id="flights_list"></div>
    </div>
`;